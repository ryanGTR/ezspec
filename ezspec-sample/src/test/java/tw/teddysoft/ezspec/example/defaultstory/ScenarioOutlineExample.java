package tw.teddysoft.ezspec.example.defaultstory;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.Feature;
import tw.teddysoft.ezspec.example.Example;
import tw.teddysoft.ezspec.extension.junit5.EzScenarioOutline;
import tw.teddysoft.ezspec.extension.junit5.Junit5Examples;
import tw.teddysoft.ezspec.table.Table;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static tw.teddysoft.ezspec.extension.junit5.JunitUtils.constructTable;

@EzFeature
public class ScenarioOutlineExample {
    static Feature feature;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("scenario outline example");
        feature.newDefaultStory();
    }

    @EzScenarioOutline
    public void scenario_outline_example_with_table_input() {
        String examples = """
                | tax_excluded | vat_rate | total_price |
                | 20000        | 0.05     | 21000       |
                | 10000        | 0.01     | 10100       |
                | 35000        | 0.10     | 38500       |
                """;

        feature.newScenarioOutline()
                .WithExamples(examples)
                .Given("the tax excluded price of a computer is <tax_excluded>", env -> {
                    env.put("tax excluded price", env.getInput().get("tax_excluded"));
                })
                .And("the VAT rate is <vat_rate>", env -> {
                    env.put("vat rate", env.getInput().get("vat_rate"));
                })
                .When("I buy the computer", env -> {
                    double price = env.geti("tax excluded price") * (1 + Double.parseDouble(env.gets("vat rate")));
                    env.put("tax included price", price);
                })
                .Then("I need to pay <total_price>", env -> {
                    assertEquals(Double.parseDouble(env.getInput().get("total_price")), env.get("tax included price", Double.class));
                })
                .Execute();
    }

    @EzScenarioOutline
    public void scenario_outline_example_with_variable_arguments() {
        feature.newScenarioOutline()
                .WithExamples(Junit5Examples.get(tax_calculation_example.class), Junit5Examples.get(zero_dollar_invoice_example.class))
                .Given("the tax excluded price of a computer is <tax_excluded>", env -> {
                    env.put("tax excluded price", env.getInput().get("tax_excluded"));
                })
                .And("the VAT rate is <vat_rate>", env -> {
                    env.put("vat rate", env.getInput().get("vat_rate"));
                })
                .When("I buy the computer", env -> {
                    double price = env.geti("tax excluded price") * (1 + Double.parseDouble(env.gets("vat rate")));
                    env.put("tax included price", price);
                })
                .Then("I need to pay <total_price>", env -> {
                    assertEquals(Double.parseDouble(env.getInput().get("total_price")), env.get("tax included price", Double.class));
                })
                .Execute();
    }

    @EzScenarioOutline
    public void scenario_outline_example_with_list_of_example() {
        List<Example> examples = new ArrayList<>();
        examples.add(Junit5Examples.get(tax_calculation_example.class));
        examples.add(Junit5Examples.get(zero_dollar_invoice_example.class));

        feature.newScenarioOutline()
                .WithExamples(examples)
                .Given("the tax excluded price of a computer is <tax_excluded>", env -> {
                    env.put("tax excluded price", env.getInput().get("tax_excluded"));
                })
                .And("the VAT rate is <vat_rate>", env -> {
                    env.put("vat rate", env.getInput().get("vat_rate"));
                })
                .When("I buy the computer", env -> {
                    double price = env.geti("tax excluded price") * (1 + Double.parseDouble(env.gets("vat rate")));
                    env.put("tax included price", price);
                })
                .Then("I need to pay <total_price>", env -> {
                    assertEquals(Double.parseDouble(env.getInput().get("total_price")), env.get("tax included price", Double.class));
                })
                .Execute();
    }

    @EzScenarioOutline
    public void moving_root_stages_and_sub_lanes() {

        feature.newScenarioOutline()
                .WithExamples(
                        Junit5Examples.get(change_root_stage_order_examples.class)
                )
                .Given("the following workflow:\n <given_workflow>", env -> {
                    // Get the nested given_workflow table
                    Table givenWorkflowTable = env.get("given_workflow", Table.class);
                })
                .When("I move <lane_name> to position <new_position> under <new_parent_name>", env -> {
                   // Execute the move lane use case
                })
                .Then("the workflow looks like the following:\n<expected_workflow>", env -> {
                    // Get the nested expected_workflow table and compare it with the resulting workflow
                }).Execute();
    }

    private static class change_root_stage_order_examples extends Junit5Examples {

        public static final List<String> HEADER = new ArrayList<>(Arrays.asList("example_code", "lane_name", "new_parent_name", "new_position", "event_count", "given_workflow", "expected_workflow"));
        public static final String WORKFLOW = "Workflow";

        @Override
        public String getDescription() {
            return "The order of root stages can be changed.";
        }

        @Override
        protected String getExamplesRawData() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Table getTable() {
            return constructTable(HEADER, provideArguments(null));
        }

        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
            String givenWorkflow = """
                | lane_name     | lane_id   |  parent_id    | lane_layout  | wip_limit  |
                | To Do         | root1     |    -1         |   Vertical   |   3        |
                | Doing         | root2     |    -1         |   Vertical   |   5        |
                | Done          | root3     |    -1         |   Vertical   |   -1       |
                """;

            String expectedRootStageToMiddle = """
                | lane_name     | lane_id   |  parent_id    | lane_layout  | wip_limit  |
                | Doing         | root2     |    -1         |   Vertical   |   5        |
                | To Do         | root1     |    -1         |   Vertical   |   3        |
                | Done          | root3     |    -1         |   Vertical   |   -1       |
                """;

            String expectedRootStageToLast
                    = """
                | lane_name     | lane_id   |  parent_id    | lane_layout  | wip_limit  |
                | Doing         | root2     |    -1         |   Vertical   |   5        |
                | Done          | root3     |    -1         |   Vertical   |   -1       |
                | To Do         | root1     |    -1         |   Vertical   |   3        |
                """;

            return Stream.of(
                    Arguments.of("ML-D01", "To Do", WORKFLOW, "1", 1, givenWorkflow, expectedRootStageToMiddle),
                    Arguments.of("ML-D02", "To Do", WORKFLOW, "2", 1, givenWorkflow, expectedRootStageToLast)
            );
        }
    }

    private static class tax_calculation_example extends Junit5Examples {
        String rawData = """
                | tax_excluded | vat_rate | total_price |
                | 20000        | 0.05     | 21000       |
                | 10000        | 0.01     | 10100       |
                | 35000        | 0.10     | 38500       |
                """;

        @Override
        public String getDescription() {
            return "Calculate tax.";
        }

        @Override
        protected String getExamplesRawData() {
            return rawData;
        }
    }

    private static class zero_dollar_invoice_example extends Junit5Examples {
        String rawData = """
                | tax_excluded | vat_rate | total_price |
                | 0            | 0.05     | 0           |
                """;

        @Override
        public String getDescription() {
            return "Zero dollar invoice.";
        }

        @Override
        protected String getExamplesRawData() {
            return rawData;
        }
    }
}
