package tw.teddysoft.ezspec.example.defaultstory;

import org.junit.jupiter.api.BeforeAll;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.Feature;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@EzFeature
public class ScenarioExample {
    static Feature feature;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Scenario example using default story");
        feature.newDefaultStory();
    }

    @EzScenario
    public void scenario_example_with_default_story() {
        feature.newScenario()
                .Given("the tax excluded price of a computer is $20000", env -> {
                    assertTrue(env.hasArgument());
                    assertEquals(1, env.getArgs().size());
                    assertEquals("20000", env.getArgs().get(0).value());
                    assertEquals("20000", env.getArg(0));

                    env.put("tax excluded price", env.getArgi(0));
                })
                .And("the VAT rate is ${VAT=0.05}", env -> {
                    assertEquals(1, env.getArgs().size());
                    assertEquals("0.05", env.getArg("VAT"));
                    assertEquals("VAT", env.getArgs().get(0).key());

                    env.put("vat rate", env.getArgd("VAT"));
                })
                .When("I buy the computer", env ->{
                    double price = env.get("tax excluded price", Integer.class) * (1 + env.get("vat rate", Double.class));
                    env.put("tax included price", price);
                })
                .Then("I need to pay ${total_price:21,000}", env ->{
                    assertEquals(env.getArgi("total_price"), env.get("tax included price", Double.class));
                })
                .Execute();
    }
}
