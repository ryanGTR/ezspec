package tw.teddysoft.ezspec.example.stories;

import org.junit.jupiter.api.BeforeAll;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.Feature;
import tw.teddysoft.ezspec.Story;
import tw.teddysoft.ezspec.example.Example;
import tw.teddysoft.ezspec.extension.junit5.EzScenarioOutline;
import tw.teddysoft.ezspec.extension.junit5.Junit5Examples;
import tw.teddysoft.ezspec.scenario.ScenarioOutline;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@EzFeature
public class ScenarioOutlineExample {
    static Feature feature;
    static Story scenarioOutlineWithTable;
    static Story scenarioOutlineWithExamples;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("scenario outline example");
        scenarioOutlineWithTable = feature.newStory("scenario outline with table");
        scenarioOutlineWithExamples = feature.newStory("scenario outline with example");
    }

    @EzScenarioOutline
    public void scenario_outline_example_with_table_input() {
        String examples = """
                | tax_excluded | vat_rate | total_price |
                | 20000        | 0.05     | 21000       |
                | 10000        | 0.01     | 10100       |
                | 35000        | 0.10     | 38500       |
                """;

        ScenarioOutline.New(scenarioOutlineWithTable)
                .WithExamples(examples)
                .Given("the tax excluded price of a computer is <tax_excluded>", env -> {
                    env.put("tax excluded price", env.getInput().get("tax_excluded"));
                })
                .And("the VAT rate is <vat_rate>", env -> {
                    env.put("vat rate", env.getInput().get("vat_rate"));
                })
                .When("I buy the computer", env -> {
                    double price = env.geti("tax excluded price") * (1 + Double.parseDouble(env.gets("vat rate")));
                    env.put("tax included price", price);
                })
                .Then("I need to pay <total_price>", env -> {
                    assertEquals(Double.parseDouble(env.getInput().get("total_price")), env.get("tax included price", Double.class));
                })
                .Execute();
    }

    @EzScenarioOutline
    public void scenario_outline_example_with_variable_arguments() {
        ScenarioOutline.New(scenarioOutlineWithExamples)
                .WithExamples(Junit5Examples.get(tax_calculation_example.class), Junit5Examples.get(zero_dollar_invoice_example.class))
                .Given("the tax excluded price of a computer is <tax_excluded>", env -> {
                    env.put("tax excluded price", env.getInput().get("tax_excluded"));
                })
                .And("the VAT rate is <vat_rate>", env -> {
                    env.put("vat rate", env.getInput().get("vat_rate"));
                })
                .When("I buy the computer", env -> {
                    double price = env.geti("tax excluded price") * (1 + Double.parseDouble(env.gets("vat rate")));
                    env.put("tax included price", price);
                })
                .Then("I need to pay <total_price>", env -> {
                    assertEquals(Double.parseDouble(env.getInput().get("total_price")), env.get("tax included price", Double.class));
                })
                .Execute();
    }

    @EzScenarioOutline
    public void scenario_outline_example_with_list_of_example() {
        List<Example> examples = new ArrayList<>();
        examples.add(Junit5Examples.get(tax_calculation_example.class));
        examples.add(Junit5Examples.get(zero_dollar_invoice_example.class));

        ScenarioOutline.New(scenarioOutlineWithExamples)
                .WithExamples(examples)
                .Given("the tax excluded price of a computer is <tax_excluded>", env -> {
                    env.put("tax excluded price", env.getInput().get("tax_excluded"));
                })
                .And("the VAT rate is <vat_rate>", env -> {
                    env.put("vat rate", env.getInput().get("vat_rate"));
                })
                .When("I buy the computer", env -> {
                    double price = env.geti("tax excluded price") * (1 + Double.parseDouble(env.gets("vat rate")));
                    env.put("tax included price", price);
                })
                .Then("I need to pay <total_price>", env -> {
                    assertEquals(Double.parseDouble(env.getInput().get("total_price")), env.get("tax included price", Double.class));
                })
                .Execute();
    }

    private static class tax_calculation_example extends Junit5Examples {
        String rawData = """
                | tax_excluded | vat_rate | total_price |
                | 20000        | 0.05     | 21000       |
                | 10000        | 0.01     | 10100       |
                | 35000        | 0.10     | 38500       |
                """;

        @Override
        public String getDescription() {
            return "Calculate tax.";
        }
        @Override
        protected String getExamplesRawData() {
            return rawData;
        }
    }

    private static class zero_dollar_invoice_example extends Junit5Examples {
        String rawData = """
                | tax_excluded | vat_rate | total_price |
                | 0            | 0.05     | 0           |
                """;

        @Override
        public String getDescription() {
            return "Zero dollar invoice.";
        }
        @Override
        protected String getExamplesRawData() {
            return rawData;
        }
    }
}
