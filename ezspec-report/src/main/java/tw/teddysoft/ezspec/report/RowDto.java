package tw.teddysoft.ezspec.report;


import tw.teddysoft.ezspec.table.Row;

import java.util.List;

public record RowDto(List<String> columns) {
    public static RowDto of(Row row){
        return new RowDto(row.columns().stream().toList());
    }

    public static List<RowDto> of(List<Row> rows){
        return rows.stream().map(RowDto::of).toList();
    }
}