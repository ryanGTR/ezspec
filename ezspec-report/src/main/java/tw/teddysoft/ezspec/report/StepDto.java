package tw.teddysoft.ezspec.report;


import tw.teddysoft.ezspec.scenario.Step;

import java.util.List;

public class StepDto {
    public String keyword;
    public String description;
    public String stepExecutionOutcome;
    public String errorMessage;

    public String exception;
    public String stackTrace;
    public boolean continuousAfterFailure;

    public StepDto(){
        exception = "";
        stackTrace = "";
    }

    public static StepDto of(Step step) {
        StepDto stepDot = new StepDto();
        stepDot.keyword = step.getName();
        stepDot.description = step.description();
        stepDot.continuousAfterFailure = step.isContinuousAfterFailure();
        stepDot.stepExecutionOutcome = step.getResult().getExecutionOutcome().name();
        stepDot.errorMessage = step.getResult().getFailureMessage();

        if (null != step.getResult().getException()){
            stepDot.exception = step.getResult().getException().toString();
            stepDot.stackTrace = step.getResult().getStackTrace();
        }

        return stepDot;
    }

    public static List<StepDto> of(List<Step> steps) {
        return steps.stream().map(StepDto::of).toList();
    }
}
