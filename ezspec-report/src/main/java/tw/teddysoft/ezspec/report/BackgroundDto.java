package tw.teddysoft.ezspec.report;


import tw.teddysoft.ezspec.scenario.Background;

import java.util.List;

public record BackgroundDto(String keyword, String name, List<StepDto> stepDtos) implements SpecificationElementDto {
    public static BackgroundDto of(Background background) {
        return new BackgroundDto(Background.KEYWORD, background.getName(), StepDto.of(background.steps()));
    }

    public static List<BackgroundDto> of(List<Background> backgrounds) {
        return backgrounds.stream().map(BackgroundDto::of).toList();
    }
}