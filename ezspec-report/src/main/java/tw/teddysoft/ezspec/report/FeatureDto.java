package tw.teddysoft.ezspec.report;

import tw.teddysoft.ezspec.Feature;

import java.util.List;

public record FeatureDto(String keyword, String name, String description, List<StoryDto> storyDots) {
    public static FeatureDto of(Feature feature){
        return new FeatureDto(Feature.KEYWORD, feature.getName(), feature.getDescription(), StoryDto.of(feature.getStories()));
    }
}
