package tw.teddysoft.ezspec.report;

import tw.teddysoft.ezspec.scenario.Scenario;
import tw.teddysoft.ezspec.scenario.ScenarioOutline;

import java.util.List;

public record ScenarioOutlineDto(
        String keyword,
        String name,
        List<StepDto> rawStepDtos,
        List<ExampleDto> allExampleDtos,
        List<ScenarioDto> runtimeScenarioDtos) implements SpecificationElementDto {

    public static SpecificationElementDto of(ScenarioOutline scenarioOutline) {
        return new ScenarioOutlineDto(
                ScenarioOutline.KEYWORD,
                scenarioOutline.getName(),
                StepDto.of(scenarioOutline.getRawSteps()),
                ExampleDto.of(scenarioOutline.getAllExamples()),
                ScenarioDto.of(scenarioOutline.RuntimeScenarios().stream().map(x -> (Scenario)x).toList()));
    }
}
