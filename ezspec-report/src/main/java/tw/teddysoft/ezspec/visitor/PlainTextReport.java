package tw.teddysoft.ezspec.visitor;


import tw.teddysoft.ezspec.Feature;
import tw.teddysoft.ezspec.Story;
import tw.teddysoft.ezspec.scenario.Background;
import tw.teddysoft.ezspec.scenario.RuntimeScenario;
import tw.teddysoft.ezspec.scenario.ScenarioOutline;
import tw.teddysoft.ezspec.scenario.Step;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import static java.lang.String.format;

/**
 * {@code PlainTextReport} is a class for generating reports.
 *
 * @author Teddy Chen
 * @since 1.0
 */
public class PlainTextReport implements SpecificationElementVisitor {
    private final StringBuilder output = new StringBuilder();

    @Override
    public void visit(SpecificationElement element) {
        switch (element){
            case Feature feature -> {
                output.append(feature.featureText());
            }
            case Story story ->  {
                output.append(format("\n\nAbility: %s\n\n%s", story.getName(), story.description()));
            }
            case Background background ->  {
                output.append(format("\n%s",background));
            }
            case ScenarioOutline scenarioOutline ->  {
                output.append(format("\n\n%s", scenarioOutline));
            }
            case RuntimeScenario runtimeScenario -> {
                if (runtimeScenario.isFromScenarioOutline()){
                    String exampleName;
                    exampleName = runtimeScenario.activeTable().get("example_code");
                    output.append(format("\n[%d] %s", Integer.valueOf(runtimeScenario.getIndex() + 1), exampleName));
                } else {
                    output.append(format("\n\nScenario: %s", runtimeScenario.getReplacedUnderscoresName()));
                }

            }
            case Step step ->  {
                output.append(format("\n[%s] %s %s", step.getResult(), step.getName(), step.description()));
                switch (step.getResult().getExecutionOutcome()){
                    case Failure -> {
                        output.append(format("\n\t\t\tat %s", step.getResult().getFailureMessage()));
                    }
                    case Pending -> {
                        if (!step.getResult().getFailureMessage().isEmpty())
                            output.append(format("\n\t\t\tcaused by %s", step.getResult().getFailureMessage()));
                    }
                    default -> {}
                }
            }
            default -> {}
        }
    }

    public String getOutput() {
        return output.toString();
    }

    public void writeToFile(String fileName){
        try (FileWriter fileWriter = new FileWriter(fileName);
             PrintWriter printWriter = new PrintWriter(fileWriter)) {
            printWriter.print(output);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
