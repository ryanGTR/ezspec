package tw.teddysoft.ezspec;

import org.junit.jupiter.api.extension.ExtendWith;
import tw.teddysoft.ezspec.extension.junit5.EzSpecReportExtension;
import tw.teddysoft.ezspec.report.EzSpecReportFormat;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@ExtendWith(EzSpecReportExtension.class)
@EzSpecReportFormat({EzSpecReportFormat.Format.json, EzSpecReportFormat.Format.txt})
public @interface EzFeatureReport {
}
