package tw.teddysoft.ezspec.extension.junit5;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.platform.commons.util.AnnotationUtils;
import tw.teddysoft.ezspec.Feature;
import tw.teddysoft.ezspec.report.DisableEzSpecReport;
import tw.teddysoft.ezspec.report.EzSpecReportFormat;
import tw.teddysoft.ezspec.report.FeatureDto;
import tw.teddysoft.ezspec.visitor.PlainTextReport;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.*;

/**
 * {@code EzSpecReportExtension} is a class for generating report.
 *
 * @author Teddy Chen
 * @since 1.0
 */
public class EzSpecReportExtension implements AfterAllCallback {

    public static final String FEATURE_NAME = "feature";

    public static String folderPrefix = "./target/ezSpec-report/";
    /**
     * To control generating report or not.
     */
    public static boolean EZREPORT;
    private static final JsonMapper mapper;

    static {
        EZREPORT = false;
        var env = System.getenv("EZREPORT");
        if ("on".equalsIgnoreCase(env)) {
            EZREPORT = true;
        }
        EZREPORT = true;
        System.out.println("EZREPORT ENV = " + env);
        System.out.println("EZREPORT = " + EZREPORT);
        mapper = JsonMapper.builder()
                .addModule(new ParameterNamesModule())
                .addModule(new Jdk8Module())
                .addModule(new JavaTimeModule())
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true)
                .build();
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
    }

    @Override
    public void afterAll(ExtensionContext extensionContext) throws Exception {

        if (!EZREPORT) return;

        Class testClass = extensionContext.getRequiredTestClass();
        if (testClass.isAnnotationPresent(DisableEzSpecReport.class)) return;

        Optional<Field> featureField = getFeatureInTestClass(testClass);
        if (featureField.isEmpty()) return;

        Set<String> formats = new HashSet<>(findReportFormatsFromTestClass(testClass));
        if (formats.isEmpty())
            formats.addAll(findReportFormatsFormInterfaces(testClass));

        featureField.get().setAccessible(true);
        Feature feature = (Feature) featureField.get().get(null);
        if (null == feature) return;

        for (var each : formats){
            String fileName = getReportFileName(extensionContext);
            switch (EzSpecReportFormat.Format.valueOf(each)){
                case txt:
                    writeTxtReport(EzSpecReportFormat.Format.valueOf(each), feature, fileName);
                    break;
                case json:
                    writeJsonReport(EzSpecReportFormat.Format.valueOf(each), feature, fileName);
                    break;
                default: //ignore
            }
        }
    }

    private Set<String> findReportFormatsFromTestClass(Class testClass){
        Set<String> formats = new HashSet<>();
        Optional<EzSpecReportFormat> annotation = AnnotationUtils.findAnnotation(testClass, EzSpecReportFormat.class);
        if (annotation.isPresent()){
            formats.addAll(Arrays.stream(annotation.get().value()).map(x -> x.toString()).toList());
        }
        return formats;
    }

    private Set<String> findReportFormatsFormInterfaces(Class testClass){
        Set<String> formats = new HashSet<>();
        Class<?>[] interfaces = testClass.getInterfaces();
        for (Class<?> interf : interfaces) {
            if (!interf.isAnnotationPresent(EzSpecReportFormat.class)) continue;
            var annotation = interf.getAnnotation(EzSpecReportFormat.class);
            formats.addAll(Arrays.stream(annotation.value()).map(x -> x.toString()).toList());
        }

        return formats;
    }

    private Optional<Field> getFeatureInTestClass(Class testClass){
        List<Field> fields = new ArrayList<>(List.of(testClass.getFields()));
        fields.addAll(List.of(testClass.getDeclaredFields()));
        for (Field each : fields) {
            if (each.getName().equals(FEATURE_NAME)) {
                return Optional.of(each);
            }
        }
        return Optional.empty();
    }

    private void writeTxtReport(EzSpecReportFormat.Format format, Feature feature, String fileName){
        if (format == EzSpecReportFormat.Format.txt){
            PlainTextReport plainTextReport = new PlainTextReport();
            feature.accept(plainTextReport);
            writeReport(folderPrefix, fileName + "." + EzSpecReportFormat.Format.txt.name(), plainTextReport.getOutput());
        }
    }

    private void writeJsonReport(EzSpecReportFormat.Format format, Feature feature, String fileName){
        if (format == EzSpecReportFormat.Format.json){
            var json = asString(FeatureDto.of(feature));
            writeReport(folderPrefix, fileName + "." + EzSpecReportFormat.Format.json.name(), json);
        }
    }

    private String getReportFileName(ExtensionContext extensionContext){
        String fileName = extensionContext.getDisplayName();
        if (extensionContext.getTestClass().isPresent()){
            fileName = extensionContext.getTestClass().get().getName();
        }
        return fileName;
    }

    static void writeReport(String folderName, String fileName, String content) {
        File directory = new File(folderName);
        if (!directory.exists()) directory.mkdirs();

        try (FileWriter fileWriter = new FileWriter(folderName + fileName);
             PrintWriter printWriter = new PrintWriter(fileWriter)) {
            printWriter.print(content);
        } catch (IOException e) {
            throw new RuntimeException(e);

        }
    }

    private String asString(Object value){
        try {
            return mapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}

