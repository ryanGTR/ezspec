# ezSpec-Report

**ezSpec-report** is an **ezSpec-core** extension that generates test reports for living documentation. When an ezSpec feature (i.e., a JUnit 5 test case) is annotated with `@EzFeatureReport`, which is defined by ezSpec-report, a plain text report and a JSON format report are automatically generated after the feature is executed.

ezSpec-report will be extended to support full funtionality of living documentation in the future.

## How to use

### Maven

```xml
<dependency>
    <groupId>tw.teddysoft.ezspec</groupId>
    <artifactId>ezspec-report</artifactId>
    <version>LATEST</version>
</dependency>
```

### Documentation

- [Javadoc](https://www.javadoc.io/doc/tw.teddysoft.ezspec/ezspec-report)

### Usage Example

Annotate a test class with `@EzFeatureReport` to generate living documents (i.e., test reports) in the format of JSON and txt.

Example:

```Java
@EzFeature
@EzFeatureReport
public class RegisterUserUseCaseTest {
  static Feature feature = Feature.New("PlainTextReport");
  
  @EzScenario
  public void write_to_file() {
    feature.newDefaultStory()
            .newScenario("scenario")
            .Given("a given", env -> {
            })
            .And("that the lead waits in the queue to the tenants pipeline", env -> {
            })
            .When("the tenant lead has a score", env -> {
            })
            .Then("we should result the lead", env -> {
            }).Execute();
  }
}
```

To temporary disable generating living documents, annotate the test class with `@DisableEzSpecReport`.

Example:

```Java
@EzFeature
@DisableEzSpecReport
@EzFeatureReport
public class RegisterUserUseCaseTest {
  static Feature feature = Feature.New("PlainTextReport");
  
  @EzScenario
  public void write_to_file() {
    feature.newDefaultStory()
            .newScenario("scenario")
            .Given("a given", env -> {
            })
            .And("that the lead waits in the queue to the tenants pipeline", env -> {
            })
            .When("the tenant lead has a score", env -> {
            })
            .Then("we should result the lead", env -> {
            }).Execute();
  }
}
```
