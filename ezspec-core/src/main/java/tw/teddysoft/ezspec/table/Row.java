package tw.teddysoft.ezspec.table;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;
import static tw.teddysoft.ezspec.extension.SpecUtils.center;
import static tw.teddysoft.ezspec.extension.SpecUtils.deleteEndWithNewLine;

/**
 * {@code Row} is a class for representing row in {@code Table}.
 *
 * @author Teddy Chen
 * @since 1.0
 */
public class Row {
    private final List<String> columns;
    private final Header header;

    public Row(Header header, List<String> columns, int index){
        this.header = header;
        this.columns = new ArrayList<>(columns);
    }

    public String get(String columnName){
        for(int i = 0; i < header.size(); i++){
            if (header.get(i).equals(columnName)){
                return columns.get(i);
            }
        }

        throw new RuntimeException("Header column '" + columnName + "' not found.");
    }

    public String getOrEmpty(String columnName){
        for(int i = 0; i < header.size(); i++){
            if (header.get(i).equals(columnName)){
                return columns.get(i);
            }
        }
        return "";
    }

    public String get(int index){
        return columns.get(index);
    }

    public List<String> columns(){
        return columns;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < columns().size(); i++){
            if (columns().get(i).isEmpty()) continue;

            if (Table.containsTable(columns().get(i))){
                sb.append(format("\n<%s>", header.get(i)));
                sb.append(format("\n%s", deleteEndWithNewLine(columns().get(i))));
            }
            else {
                var headerLength = ("|\t" +  header.get(i) + "\t|").length();
                sb.append(format("%s\t|", center(columns().get(i), headerLength)));
            }
        }

        return sb.toString();
    }
}
