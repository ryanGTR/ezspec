package tw.teddysoft.ezspec.scenario;

/**
 * {@code ConcurrentGroup} is an interface for executing with
 * {@code And} and {@code But}.
 *
 * @author Teddy Chen
 * @since 1.0
 */
public interface ConcurrentGroup  {
}
