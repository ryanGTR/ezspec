package tw.teddysoft.ezspec.scenario;

import org.junit.jupiter.api.DynamicNode;
import tw.teddysoft.ezspec.Story;
import tw.teddysoft.ezspec.exception.EzSpecError;
import tw.teddysoft.ezspec.table.Table;
import tw.teddysoft.ezspec.visitor.SpecificationElement;

import java.util.*;
import java.util.function.Consumer;

import static java.lang.String.format;
import static tw.teddysoft.ezspec.extension.SpecUtils.getReplacedUnderscores;

/**
 * {@code Scenario} is a class for representing Gherkin scenario.
 *
 * @author Teddy Chen
 * @since 1.0
 */
public abstract class Scenario implements SpecificationElement {

    public static final String KEYWORD = "Scenario";
    private final String name;
    private final List<Step> steps;
    protected Table lookupTable;
    protected ScenarioEnvironment runtime;
    protected int index;
    public static final String ARG = "$ARGUMENT";

    /**
     * Gets scenario environment of this scenario.
     *
     * @return the scenario environment
     */
    protected ScenarioEnvironment getEnvironment(){
        return runtime;
    }

    /**
     * Creates a scenario in story.
     *
     * @param story the story
     * @return the scenario
     */
    public static Scenario New(Story story){
        Objects.requireNonNull(story, "story");

        return story.newScenario(getEnclosingMethodName());
    }

    /**
     * Creates a scenario in story.
     *
     * @param name  the name of scenario
     * @param story the story
     * @return the scenario
     */
    public static Scenario New(String name, Story story){
        Objects.requireNonNull(name, "name");
        Objects.requireNonNull(story, "story");

        return story.newScenario(name);
    }

    /**
     * Instantiates a new Scenario.
     *
     * @param name the name
     */
    public Scenario(String name){
        this(name, new Table());
    }

    /**
     * Instantiates a new Scenario.
     *
     * @param name       the name of this scenario
     * @param background the background of this scenario
     */
    public Scenario(String name, Background background){
        Objects.requireNonNull(name, "name");
        Objects.requireNonNull(background, "background");

        this.name = name;
        steps = new ArrayList<>();
        this.runtime = ScenarioEnvironment.clone(background.getEnvironment());
        this.lookupTable = background.activeTable();
    }

    /**
     * Instantiates a new Scenario.
     *
     * @param name  the name of this scenario
     * @param table the table of this scenario
     */
    public Scenario(String name, Table table){
        Objects.requireNonNull(name, "name");
        Objects.requireNonNull(table, "table");

        this.name = name;
        steps = new ArrayList<>();
        this.runtime = ScenarioEnvironment.create();
        this.runtime.setInput(table);
        lookupTable = table;
    }

    /**
     * Gets active table of this scenario.
     *
     * @return the table
     */
    public Table activeTable(){
        return lookupTable;
    }

    public String getName() {
        return name;
    }

    /**
     * The method for representing Gherkin Given step of this scenario.
     *
     * @param description the description of Given step
     * @param continuous  the parameter for deciding to continue executing
     *                    the next step after this Given step failed
     * @param callback    a lambda expression to represent the implementation
     *                    of this Given step
     * @return this scenario
     */
//region :Keywords
    public abstract Scenario Given(String description, boolean continuous, Consumer<ScenarioEnvironment> callback);

    /**
     * The method for representing Gherkin Given step of this scenario.
     *
     * @param description the description of Given step
     * @param callback    a lambda expression to represent the implementation
     *                    of this Given step
     * @return this scenario
     */
    public abstract Scenario Given(String description, Consumer<ScenarioEnvironment> callback);

    /**
     * The method for representing Gherkin When step of this scenario.
     *
     * @param description the description of When step
     * @param continuous  the parameter for deciding to continue executing
     *                    the next step after this When step failed
     * @param callback    a lambda expression to represent the implementation
     *                    of this When step
     * @return this scenario
     */
    public abstract Scenario When(String description, boolean continuous, Consumer<ScenarioEnvironment> callback);

    /**
     * The method for representing Gherkin When step of this scenario.
     *
     * @param description the description of When step
     * @param callback    a lambda expression to represent the implementation
     *                    of this When step
     * @return this scenario
     */
    public abstract Scenario When(String description, Consumer<ScenarioEnvironment> callback);

    /**
     * The method for representing Gherkin Then step of this scenario.
     *
     * @param description the description of Then step
     * @param continuous  the parameter for deciding to continue executing
     *                    the next step after this Then step failed
     * @param callback    a lambda expression to represent the implementation
     *                    of this Then step
     * @return this scenario
     */
    public abstract Scenario Then(String description, boolean continuous, Consumer<ScenarioEnvironment> callback);

    /**
     * The method for representing Gherkin Then step of this scenario.
     *
     * @param description the description of Then step
     * @param callback    a lambda expression to represent the implementation
     *                    of this Then step
     * @return this scenario
     */
    public abstract Scenario Then(String description, Consumer<ScenarioEnvironment> callback);

    /**
     * The method for representing Gherkin Scenario execution succeeded before
     * asserting following Then steps.
     *
     * @param continuous  the parameter for deciding to continue executing
     *                    the next step after this Then step failed
     * @param callback    a lambda expression to represent the implementation
     *                    of this Then step
     * @return this scenario
     */
    public abstract Scenario ThenSuccess(boolean continuous, Consumer<ScenarioEnvironment> callback);

    /**
     * The method for representing Gherkin Scenario execution succeeded before
     * asserting following Then steps.
     *
     * @param callback    a lambda expression to represent the implementation
     *                    of this Then step
     * @return this scenario
     */
    public abstract Scenario ThenSuccess(Consumer<ScenarioEnvironment> callback);

    /**
     * The method for representing Gherkin Scenario execution succeeded before
     * asserting following Then steps.
     *
     * @param description the description of ThenSuccess step
     * @param callback    a lambda expression to represent the implementation
     *                    of this Then step
     * @return this scenario
     */
    public abstract Scenario ThenSuccess(String description, Consumer<ScenarioEnvironment> callback);

    /**
     * The method for representing Gherkin Scenario execution succeeded before
     * asserting following Then steps.
     *
     * @param description the description of ThenSuccess step
     * @param continuous  the parameter for deciding to continue executing
     *                    the next step after this Then step failed
     * @param callback    a lambda expression to represent the implementation
     *                    of this Then step
     * @return this scenario
     */
    public abstract Scenario ThenSuccess(String description, boolean continuous, Consumer<ScenarioEnvironment> callback);

    /**
     * The method for representing Gherkin Scenario execution failed before
     * asserting following Then steps.
     *
     * @param callback    a lambda expression to represent the implementation
     *                    of this Then step
     * @return this scenario
     */
    public abstract Scenario ThenFailure(Consumer<ScenarioEnvironment> callback);

    /**
     * The method for representing Gherkin Scenario execution failed before
     * asserting following Then steps.
     *
     * @param continuous  the parameter for deciding to continue executing
     *                    the next step after this Then step failed
     * @param callback    a lambda expression to represent the implementation
     *                    of this Then step
     * @return this scenario
     */
    public abstract Scenario ThenFailure(boolean continuous, Consumer<ScenarioEnvironment> callback);

    /**
     * The method for representing Gherkin Scenario execution failed before
     * asserting following Then steps.
     *
     * @param description the description of ThenFailure step
     * @param continuous  the parameter for deciding to continue executing
     *                    the next step after this Then step failed
     * @param callback    a lambda expression to represent the implementation
     *                    of this Then step
     * @return this scenario
     */
    public abstract Scenario ThenFailure(String description, boolean continuous, Consumer<ScenarioEnvironment> callback);

    /**
     * The method for representing Gherkin Scenario execution failed before
     * asserting following Then steps.
     *
     * @param description the description of ThenFailure step
     * @param callback    a lambda expression to represent the implementation
     *                    of this Then step
     * @return this scenario
     */
    public abstract Scenario ThenFailure(String description, Consumer<ScenarioEnvironment> callback);

    /**
     * The method for representing Gherkin And step.
     *
     * @param description the description of And step
     * @param callback    a lambda expression to represent the implementation
     *                    of this And step
     * @return this scenario
     */
    public abstract Scenario And(String description, Consumer<ScenarioEnvironment> callback);

    /**
     * The method for representing Gherkin And step.
     *
     * @param description the description of And step
     * @param continuous  the parameter for deciding to continue executing
     *                    the next step after this And step failed
     * @param callback    a lambda expression to represent the implementation
     *                    of this And step
     * @return this scenario
     */
    public abstract Scenario And(String description, boolean continuous, Consumer<ScenarioEnvironment> callback);

    /**
     * The method for representing Gherkin But step.
     *
     * @param description the description of But step
     * @param callback    a lambda expression to represent the implementation
     *                    of this But step
     * @return this scenario
     */
    public abstract Scenario But(String description, Consumer<ScenarioEnvironment> callback);

    /**
     * The method for representing Gherkin But step.
     *
     * @param description the description of But step
     * @param continuous  the parameter for deciding to continue executing
     *                    the next step after this But step failed
     * @param callback    a lambda expression to represent the implementation
     *                    of this But step
     * @return this scenario
     */
    public abstract Scenario But(String description, boolean continuous, Consumer<ScenarioEnvironment> callback);
    //endregion :Keywords

    /**
     * A static method for formatting the name.
     *
     * @param originName the origin name
     * @return the string
     */
    protected static String replaceName(String originName){
        return originName.replace("_", " ")
                .replace("$dot$", ".")
                .replace("$parenthesis$", "()")
                .replace("$comma$", ",");

    }

    /**
     * Gets test case enclosing name.
     *
     * @return the test case enclosing name
     */
    public static String getEnclosingMethodName() {
        StackWalker walker = StackWalker.getInstance();
        Optional<String> methodName = walker.walk(frames -> frames
                .skip(2)
                .findFirst()
                .map(StackWalker.StackFrame::getMethodName));

        var cookedMethodName = methodName.get();
        if (cookedMethodName.startsWith("lambda$")){
            cookedMethodName = cookedMethodName.replace("lambda$", "");
            cookedMethodName = cookedMethodName.substring(0, cookedMethodName.lastIndexOf("$"));
        }
        return cookedMethodName;
    }

    /**
     * Gets steps in this scenario.
     *
     * @return the steps
     */
    public List<Step> steps(){
        return Collections.unmodifiableList(steps);
    }

    /**
     * Gets steps in this scenario.
     *
     * @return the steps
     */
    protected List<Step> getSteps() {
        return steps;
    }

    /**
     * Gets the scenario name with underscores replaced by space.
     *
     * @return the replaced name
     */
    public String getReplacedUnderscoresName(){
        return getReplacedUnderscores(getName());
    }

    /**
     * Execute the Given/When/Then step and
     * its following And/But steps concurrently.
     */
    public abstract void ExecuteConcurrently();

    /**
     * Execute scenario with Junit{@code DynamicNode}.
     *
     * @return the dynamic node
     */
    public abstract DynamicNode DynamicExecute();

    /**
     * Execute steps in Junit{@code DynamicNode}.
     *
     * @param step the step
     * @throws Throwable the throwable when step failed
     */
    protected void dynamicExecuteStep(Step step) throws Throwable {
        if (step.getResult().isSuccess()) return;

        switch (step.getResult().getExecutionOutcome()) {
            case Pending : return;
            case Success : return;
            case Failure : throw step.getResult().getException();
            case Skipped : return;
            default : {}
        }
    }

    /**
     * Execute this scenario.
     */
    public abstract void Execute();

    /**
     * Builds spec errors as an {@code EzSpecError}.
     *
     * @param steps the steps
     * @return the {@code EzSpecError}
     */
    protected Optional<EzSpecError> buildSpecError(List<Step> steps){
        List<Throwable> throwables = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        int errorCount = 0;
        for(var step : steps){
            if (step.getResult().isFailure()) {
                errorCount = errorCount + 1;
                throwables.add(step.getResult().getException());
                var failureMessage = step.getResult().getFailureMessage();
                sb.append(format("\n[%d] %s", errorCount, failureMessage));
            }
        }

        if (throwables.isEmpty()){
            return Optional.empty();
        }

        return Optional.of(new EzSpecError(sb.toString()));
    }
}
