package tw.teddysoft.ezspec.scenario;

import java.util.function.Consumer;

/**
 * {@code But} is a class for representing Gherkin But keyword.
 *
 * @author Teddy Chen
 * @since 1.0
 */
public class But extends Step {

    /**
     * Instantiates a new But step.
     *
     * @param description the step description
     * @param continuous  the parameter for deciding to continue executing
     *                    the next step after this step failed
     * @param callback    the step definition
     */
    public But(String description, boolean continuous, Consumer callback){
        super(description, continuous, callback);
    }

    @Override
    public String getName() {
        return "But";
    }
}
