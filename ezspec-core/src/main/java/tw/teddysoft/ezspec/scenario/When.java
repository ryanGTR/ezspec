package tw.teddysoft.ezspec.scenario;

import java.util.function.Consumer;

/**
 * {@code When} is a class for representing Gherkin When keyword.
 *
 * @author Teddy Chen
 * @since 1.0
 */
public class When extends Step implements ConcurrentGroup {

    /**
     * Instantiates a new When step.
     *
     * @param description the step description
     * @param continuous  the parameter for deciding to continue executing
     *                    the next step after this step failed
     * @param callback    the step definition
     */
    public When(String description, boolean continuous, Consumer callback){
        super(description, continuous, callback);
    }

    @Override
    public String getName() {
        return "When";
    }
}
