package tw.teddysoft.ezspec.scenario;

import java.util.function.Consumer;

/**
 * {@code ThenFailure} is a class for representing the test steps are
 * executed incorrectly.
 *
 * @author Teddy Chen
 * @since 1.0
 */
public class ThenFailure extends Step implements ConcurrentGroup {

    /**
     * Instantiates a new ThenFailure step.
     *
     * @param description the step description
     * @param continuous  the parameter for deciding to continue executing
     *                    the next step after this step failed
     * @param callback    the step definition
     */
    public ThenFailure(String description, boolean continuous, Consumer callback){
        super(description, continuous, callback);
    }

    @Override
    public String getName() {
        return "Then failure";
    }
}
