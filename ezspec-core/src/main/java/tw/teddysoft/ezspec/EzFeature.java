package tw.teddysoft.ezspec;


import org.apiguardian.api.API;
import org.junit.jupiter.api.*;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
@Tag(EzSpecTag.LivingDoc.EzSpec)
@Documented
@API(
        status = API.Status.EXPERIMENTAL,
        since = "1.0"
)
public @interface EzFeature {
}
