package tw.teddysoft.ezspec.exception;

import java.util.ArrayList;
import java.util.List;

/**
 * {@code EzSpecError} will be thrown when step execution is failed.
 *
 * @author Teddy Chen
 * @since 1.0
 */
public class EzSpecError extends Error{

    public EzSpecError(List<Throwable> throwables, String message){
        super(message);
        ArrayList<StackTraceElement> newStackTract = new ArrayList<>();
        throwables.stream().forEach( ex -> {
                    int stackIndex = 0;
                    StackTraceElement stackTrace [] = ex.getStackTrace();
                    for(int i = 0; i < stackTrace.length; i++){
                        if (stackTrace[i].toString().startsWith("tw.teddysoft.ezspec.scenario.RuntimeScenario.invokeStep")){
                            stackIndex = i - 1;
                        }
                    }
                    newStackTract.add(stackTrace[stackIndex]);
                }
        );
        this.setStackTrace(newStackTract.toArray(new StackTraceElement[0]));
    }

    public EzSpecError(String str){
        super(str);
    }

    public EzSpecError(Throwable throwable){
        super(throwable);
    }
}
