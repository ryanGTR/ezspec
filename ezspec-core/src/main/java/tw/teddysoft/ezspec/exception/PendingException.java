package tw.teddysoft.ezspec.exception;

/**
 * {@code PendingException} will be thrown when step has not been implemented.
 *
 * @author Teddy Chen
 * @since 1.0
 */
public class PendingException extends RuntimeException {

    public PendingException(String message){
        super(message);
    }

    public PendingException(){
        super();
    }

    public static void pending(){
        throw new PendingException();
    }

    public static void pending(String message){
        throw new PendingException(message);
    }
}
