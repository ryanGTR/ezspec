package tw.teddysoft.ezspec.example;



import tw.teddysoft.ezspec.table.Table;

import java.util.Arrays;
import java.util.Objects;

import static java.lang.String.format;

/**
 * {@code Example} is a class for representing Gherkin examples.
 *
 * @author Teddy Chen
 * @since 1.0
 */
public class Example implements Examples {
    private final String name;
    private final String description;
    private Table table;

    public Example(String tableContent) {
        this("", "", tableContent);
    }

    public Example(String name, String description, String tableContent) {
        Objects.requireNonNull(name, "name");
        Objects.requireNonNull(description, "description");
        Objects.requireNonNull(tableContent, "table content");

        this.name = name;
        this.description = description;
        table = new Table(tableContent);
    }

    public Example(Example that) {
        Objects.requireNonNull(that, "that");

        this.name = that.getName();
        this.description = that.getDescription();
        this.table = new Table(that.getTable());
    }

    Example(String name, String description, Table table) {
        Objects.requireNonNull(name, "name");
        Objects.requireNonNull(description, "description");
        Objects.requireNonNull(table, "table");

        this.name = name;
        this.description = description;
        this.table = new Table(table);
    }

    public void clear(){
        table.clear();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public Table getTable() {
        return table;
    }

    @Override
    public Example getExample() {
        return this;
    }

    /**
     * Extracts a row from as table of this example and transform to a table.
     *
     * @param i the row index
     * @return the table
     */
    public Table rowAsTable(int i) {
        return new Table(table.header(), Arrays.asList(table.rows().get(i)));
    }

    /**
     * Examples text string including example name, example description
     * and example table.
     *
     * @return the string
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(format("\nExamples: %s\n", name));
        if (!description.isEmpty())
            sb.append(format("%s\n", description));

        sb.append(table.toString());

        return sb.toString();
    }

}
