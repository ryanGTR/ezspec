package tw.teddysoft.ezspec.visitor;

/**
 * {@code SpecificationElement} is the interface for accepting visitor to visit.
 *
 * @author Teddy Chen
 * @since 1.0
 */
public interface SpecificationElement {

    String getName();

    void accept(SpecificationElementVisitor visitor);
}
