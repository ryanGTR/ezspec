package tw.teddysoft.ezspec;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.scenario.Argument;

import static org.junit.jupiter.api.Assertions.*;

@EzFeature
public class ArgumentSpec {
    static Feature feature;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Argument");
    }

    @Test
    public void dummy(){}

    @Nested
    @Order(1)
    class UnitTest {
        @Test
        public void value_arguments_start_with_$(){
            assertEquals("1", Argument.create("$1").value());
            assertEquals("", Argument.create("$1").key());
            assertEquals("80", Argument.create("$80").value());
            assertEquals("\\$5", Argument.create("$\\$5").value());
            assertEquals("$5", Argument.create("$$5").value());
            assertEquals("$$5", Argument.create("$$$5").value());
            assertEquals("$$20$20", Argument.create("$$$20$20").value());
            assertEquals("vat:100", Argument.create("$vat:100").value());
            assertEquals("vat=100", Argument.create("$vat=100").value());
        }

        @Test
        public void key_value_arguments_place_in_$_curly_brackets(){
            assertEquals("21,000", Argument.create("${price:21,000}").value());
            assertEquals("price", Argument.create("${price : 21,000}").key());
            assertEquals("21,000", Argument.create("${price= 21,000}").value());
            assertEquals("price", Argument.create("${price=21,000}").key());
        }
    }

    @Nested
    @Order(100)
    class UsageExample{
        static Story usageExample;
        static String storyName = "Show how to specify arguments in steps of scenarios";

        @BeforeAll
        public static void beforeAll() {
            usageExample = feature.newStory(storyName, 3)
                    .description("""
                In order to demonstrate the purpose of arguments 
                As an ezBehave developer
                I want to show an example""");
        }

        @EzScenario
        public void specify_arguments_in_steps() {
            feature.withStory(storyName).newScenario()
                    .Given("the tax excluded price of a computer is $20,000", env -> {
                        assertTrue(env.hasArgument());
                        assertEquals(1, env.getArgs().size());
                        assertEquals("20,000", env.getArgs().get(0).value());
                        assertEquals("20,000", env.getArg(0));
                    })
                    .And("the VAT rate is ${VAT=5%}", env -> {
                        assertEquals(1, env.getArgs().size());
                        assertEquals("5%", env.getArg("VAT"));
                        assertEquals("VAT", env.getArgs().get(0).key());
                    })
                    .When("I buy the computer", env ->{})
                    .Then("I need to pay ${total_price:21,000}, which contains $$1,000 VAT and I get $50 credit points and a free gift of ${gift =$$$keyboard}", env ->{
                        assertEquals("21,000", env.getArg("total_price"));
                        assertEquals("$1,000", env.getArg(1));
                        assertEquals("50", env.getArg(2));
                        assertEquals("$$$keyboard", env.getArg(3));
                        assertEquals("$$$keyboard", env.getArg("gift"));
                    }).Execute();

            String expectedFullText = """
                    Scenario: specify arguments in steps
                    Given the tax excluded price of a computer is 20,000
                    And the VAT rate is 5%
                    When I buy the computer
                    Then I need to pay 21,000, which contains $1,000 VAT and I get 50 credit points and a free gift of $$$keyboard
                    """;
            assertEquals(expectedFullText, usageExample.lastScenario().toString());
        }

        @EzScenario
        public void specify_argument_with_space_after_dollar_sign_should_not_become_an_argument() {
            feature.withStory(storyName).newScenario()
                    .Given("a $ without value", env -> {
                        assertFalse(env.hasArgument());
                    }).Execute();
        }
    }
}
