package tw.teddysoft.ezspec;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import tw.teddysoft.ezspec.EzSpecTag;
import tw.teddysoft.ezspec.Feature;
import tw.teddysoft.ezspec.extension.junit5.Junit5Examples;
import tw.teddysoft.ezspec.extension.junit5.JunitUtils;
import tw.teddysoft.ezspec.table.Table;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static tw.teddysoft.ezspec.extension.junit5.JunitUtils.constructTable;

@Tag(EzSpecTag.LivingDoc.EzSpec)
public class Junit5ExtensionSpec {
    static Feature feature;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Junit5ExtensionSpec");
    }

    @Test
    public void get_an_argument_from_Junit5Examples() {
        Arguments arg = JunitUtils.getArgument(change_root_stage_order_examples.class, "ML-D03");
        assertEquals(7, arg.get().length);
        assertEquals("ML-D03", arg.get()[0]);
        assertEquals("Done", arg.get()[1]);
        assertEquals("Workflow", arg.get()[2]);
    }

    public static class change_root_stage_order_examples extends Junit5Examples {

        public static final List<String> HEADER = new ArrayList<>(Arrays.asList("example_code", "lane_name", "new_parent_name", "new_position", "event_count", "given_workflow", "expected_workflow"));
        public static final String WORKFLOW = "Workflow";

        @Override
        public String getName() {
            return "Change root stage order";
        }

        @Override
        public String getDescription() {
            return "The order of root stages can be changed.";
        }

        @Override
        protected String getExamplesRawData() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Table getTable() {
            return constructTable(HEADER, provideArguments(null));
        }

        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
            String givenWorkflow = """
                    | lane_name     | lane_id   |  parent_id    | lane_layout  | wip_limit  |
                    | To Do         | root1     |    -1         |   Vertical   |   3        |
                    | Doing         | root2     |    -1         |   Vertical   |   5        |
                    | Done          | root3     |    -1         |   Vertical   |   -1       |
                    """;

            String expectedRootStageToMiddle = """
                    | lane_name     | lane_id   |  parent_id    | lane_layout  | wip_limit  |
                    | Doing         | root2     |    -1         |   Vertical   |   5        |
                    | To Do         | root1     |    -1         |   Vertical   |   3        |
                    | Done          | root3     |    -1         |   Vertical   |   -1       |
                    """;

            String expectedRootStageToLast
                    = """
                    | lane_name     | lane_id   |  parent_id    | lane_layout  | wip_limit  |
                    | Doing         | root2     |    -1         |   Vertical   |   5        |
                    | Done          | root3     |    -1         |   Vertical   |   -1       |
                    | To Do         | root1     |    -1         |   Vertical   |   3        |
                    """;

            String expectedRootStageToFirst
                    = """
                    | lane_name     | lane_id   |  parent_id    | lane_layout  | wip_limit  |
                    | Done          | root3     |    -1         |   Vertical   |   -1       |
                    | To Do         | root1     |    -1         |   Vertical   |   3        |
                    | Doing         | root2     |    -1         |   Vertical   |   5        |
                    """;

            String expectedRootStageToSamePosition
                    = """
                    | lane_name     | lane_id   |  parent_id    | lane_layout  | wip_limit  |
                    | To Do         | root1     |    -1         |   Vertical   |   3        |
                    | Doing         | root2     |    -1         |   Vertical   |   5        |
                    | Done          | root3     |    -1         |   Vertical   |   -1       |
                    """;
            return Stream.of(
                    Arguments.of("ML-D01", "To Do", WORKFLOW, "1", 1, givenWorkflow, expectedRootStageToMiddle),
                    Arguments.of("ML-D02", "To Do", WORKFLOW, "2", 1, givenWorkflow, expectedRootStageToLast),
                    Arguments.of("ML-D03", "Done", WORKFLOW, "0", 1, givenWorkflow, expectedRootStageToFirst),
                    Arguments.of("ML-D04", "To Do", WORKFLOW, "0", 0, givenWorkflow, expectedRootStageToSamePosition)
            );
        }
    }
}
